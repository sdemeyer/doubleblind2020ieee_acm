# README #

### What? ###

This is a LaTeX template for producing articles in ACM or IEEE conference proceedings format. It provides conditional compilation settings for controlling the typical appearances one needs (i.e. double blind reviewing, annotations on / of)

* Version: 1.0
* Date: April 5th, 2020

### Features ###

* Conditional compilation settings controlling the typical appearances one needs (i.e. double blind reviewing, annotations on / of)

* Allows to switch between ACM and IEEE formatting styles

* Typical structure for empirical papers is pre-set up (i.e. RelatedWork gives an overview of the state of the art in �.. Exp_setup describes the case study set-up, which naturally leads to Results_Discussion reporting the results. Threats to Validity enumerates the threats to validity to conclude the paper in Conclusion.)


### How do I get set up? ###

* Clone this directory from git.
* Rename the file 'author2020xxx' to represent what you are writing (the family name of the first author, the year of publication and the acronym of the conference)
* rework all *TEX root = author2020xxx.tex* setting in each of the .tex include files
* Adapt the Makefile accordingly (i.e. rename 'main' and replace all occurrences of 'author2020xxx')
* Choose between ACM and IEEE formatting styles. In the first line of the main file select the appropriate document class and comment the other one out by preceeding it with a percentage character
	+ documentclass ... IEEEtran
	+ documentclass ... acmart
* Toggle the corresponding switch in 'useconditional.tex'
	+  setboolean acmtemplate true or false
* Add appropriate annotations for the various people that will write / review in 'usecommands.tex'
	+ copy / paste the newcommand OneAuthor ... lines
* Rework the commands for self references and *make sure the entries correspond with the mybib.bib file*. The idea is that for each self reference that should be anonymised you create two entries: ''xxx" and ''xxxanonymous". Then in the 'usecommands.tex' file you create a command that switches between the two depending on the  'anonymous' boolean. All that is left to do is to include this switch within the cite. See as follows.
	+ newcommand LaghariASE ... \cite{\LaghariASE}
	+ newcommand LaghariIWPSE ... \cite{\LaghariIWPSE}
	+ newcommand Rompaey ...  \cite{\Rompaey}
* Fill in author names, title abstract, ...
* commit into a new git repository
* ... and write the paper

### Contribution guidelines ###


### Who do I talk to? ###

* Serge Demeyer (University of Antwerp)