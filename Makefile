.PHONY: clean all

main = author2020xxx
sources = author2020xxx.tex author2020xxxMain.tex
includes = usepackages.tex useconditional.tex usecommands.tex usesettings.tex

author2020xxx.pdf : $(sources) $(includes) 
	pdflatex author2020xxx.tex

all:
	pdflatex $(main)
	bibtex $(main)
	pdflatex $(main)
	pdflatex $(main)

clean: 
	rm -f *.pdf
	rm -f *.aux
	rm -f *.bbl
	rm -f *.out
	rm -f *.log
	rm -f *.blg

aux: 
	rm -f *.aux
	rm -f *.out
	rm -f *.log
	rm -f *.blg
